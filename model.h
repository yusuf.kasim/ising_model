#ifndef MODEL_H_INCLUDED
#define MODEL_H_INCLUDED
#include <iostream>
#include <vector>
#include <cstdlib>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <random>
#include <queue>
#include <deque>
#include <list>
#include "particule.h"

using namespace std;

class model
{
private:
    int d; //the size of the lattice (d*d)
    double E; //the energy
    int J; //the J term that dictates the type of magnetization in the hamiltonian
    vector<vector<particle>> lattice; // the lattice structure on which the particles sit on
    vector<vector<particle>> BU; // a backup lattice with the initial conditions
    double mag; // total magnetization of the system
    double Chi; // magnetic susceptibility
    double Cv; // specific heat
    double beta; // the inverse of the temperature (Here Boltzmann constant is taken as zero)
    int itterations; // the number of times each calculation is repeated
    double h; // the H term in the hamiltonian which corresponds to an external magnetic field
    vector<double> mt; //used in calculating the autocorrelation function and time
    double em, em2, mm , mm2, dt; //average energy, energy squared, magnetization and magnetization squared, with time step for later calculations
    double clusterprob; //probability to link two particles in a cluster
    vector<vector<int>> cluster; // old not used
    queue<particle> CL; // a queue for the clustering algorithm
    double ediff; // the difference in energy between two steps
    int Csize; // the size of the cluster

public:
    model(int,vector<particle>,int,double,int,double);
    model();
    model(const model &);
    ~model();
    void init(int,vector<particle>,int,double,int,double); //not defined
    void Calcul_E_T(); //calculates the total energy
    void Calcul_M_T(); // calculates the total spin
    vector<int> Nbr(particle,int); // function giving the nearest neighbors
    double GetE() const {return E;} // gives back the private variable E, Never used
    double Calcul_E_S(particle); //calculates the energy for a single particle
    double cal_chi(int); //used in calculating the autocor, i can't recall where did I find the expression used here, probably false
    void Calculate_all(); //calculation using the metropolis algorithm
    void Calculate_dif_B(); //metropolis algorithm while changing Beta
    void Calculate_dif_B_heat_bath(); //Heat bath algorithm while changing Beta
    void Calculate_dif_B_Cluster(); // Cluster algorithm while changing Beta
    void Calculate_dif_T_Cluster(); // Cluster algorithm while changing the temperature
    void Calculate_dif_H(); //metropolis algorithm while changing H
    void Calculate_dif_H_heat_bath(); // heat bath algorithm while changing H
    void Calculate_dif_H_Cluster(); // Cluster algorithm while changing H, does not work need to recalculate the probabilities
    void flipping(); //flipping algorithm for metropolis
    double flipping_proba(particle); // metropolis flipping probability
    void print_lattice(); // prints out the lattice configuration, use pringpng for better results
    void Cal_Heat_Bath(); //calculation using heat bath algorithm
    void flipping_heat_bath(); //flipping algorithm for heat bath
    double proba_heat_bath(particle); //probability for flipping for heat bath
    void clustering(particle); // cluster algorithm
    void clusteringNJ(particle); // cluster algorithm with negative J
    void Calculate_cluster(); //calculation using Cluster algorithm
    void show_cluster(); //not used
    void printpng(); //needs to be used with a freopen function and the results to be directed at a ppm file to get a photo
    void calc_fors(); //structure factor calculations, the results can be later interprated by heat map
    double forms_S(particle); //used with the above function to find the structure factor
};


#endif // MODEL_H_INCLUDED
