#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <random>
#include "particule.h"
#include "model.h"
#include <vector>


double randd(double, double);
int rand_int(int, int);
vector<int> chain_spin(int);



#endif // FUNCTIONS_H_INCLUDED

#ifndef M_PI
#define M_PI    3.14159265358979323846
#endif // M_PI

