#include <iostream>
#include <cstdlib>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <random>
#include "particule.h"
#include "model.h"
#include "functions.h"



using namespace std;

int main()
{

    int dim,j,ittr,TY,DBH;
    double beta,H;

    ifstream inp("input.txt"); //the input file contains the following, the algorithm (1 metropolis, 2 heat bath, 3 cluster), the type of calculation (1 normal, 2 changing beta, 3 changing H), the number of iterations, size of lattice, J, Beta, H

    inp >> TY;
    inp >> DBH;
    inp >> ittr;
    inp >> dim;
    inp >> j;
    inp >> beta;
    inp >> H;

    vector<int> chain(dim*dim);

    chain = chain_spin(dim*dim);


    vector<particle> base(dim*dim);

    for (int i=0;i<(dim*dim);i++){

        base[i].SetSp(chain[i]);
    }


    srand(13);

    model test1 = model(dim,base,j,beta,ittr,H); // the arguments here are as follows, the size of lattice (d*d), the chain of particles, the J in the hamiltonian (1 for ferromagnetisme and -1 for anti ferromagnetisme), beta(1/KT), the number of itterations, and the outside magnetic field

    freopen("start.ppm","w",stdout);
    test1.printpng();

    //test1.calc_fors();

    if (TY == 1){
        if(DBH==1){test1.Calculate_all();}
        if(DBH==2){test1.Calculate_dif_B();}
        if(DBH==3){test1.Calculate_dif_H();}
    }
    if (TY == 2){
        if(DBH==1){test1.Cal_Heat_Bath();}
        if(DBH==2){test1.Calculate_dif_B_heat_bath();}
        if(DBH==3){test1.Calculate_dif_H_heat_bath();}
    }
    if (TY == 3){
        if(DBH==1){test1.Calculate_cluster();}
        if(DBH==2){test1.Calculate_dif_T_Cluster();}  //make sure to change to calculate_dif_B later !!
        if(DBH==3){test1.Calculate_dif_H_heat_bath();}
    }

    freopen("end.ppm","w",stdout);
    test1.printpng();

    test1.calc_fors();

    //cout << M_PI << endl;

    return EXIT_SUCCESS;
}



/*while(k<50){
  ifstream myfile("file_no_" + std::to_string(k) + ".vtk");
  // myfile << "data to write\n";
  k++;
}*/ //using int in filename
