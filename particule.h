#ifndef PARTICULE_H_INCLUDED
#define PARTICULE_H_INCLUDED
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <random>


class particle
{
private:
    int spin;   //the spin, positions in real and reciprocal lattice and a check for the cluster algorithm
    int posx;
    int posy;
    int inq;
    double kx;
    double ky;

public:
    particle(int);
    particle(); //this is not defined
    particle(const particle &);
    ~particle();
    void SetSp(int);
    void Set_XY(int,int);
    void Set_K(double,double);
    void Set_INQ(int);
    int GetSp() const {return spin;}
    int GetX() const {return posx;}
    int GetY() const {return posy;}
    int GetINQ() const {return inq;}
    double GetKx() const {return kx;}
    double GetKy() const {return ky;}
    void flip_s();  // flip the spin
    int flip_v(); // returns the value of a flipped spin, I never used it
};

#endif // PARTICULE_H_INCLUDED
