#include <iostream>
#include <cstdlib>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <random>
#include "particule.h"
#include "model.h"
#include "functions.h"
#include <vector>

using namespace std;


double randd(double mm, double MM){
    double random= ((double)rand())/(double)RAND_MAX;
    double range = MM - mm;
    return (random*range)+mm;
} // a function used to randomly generate real numbers

int rand_int(int mm, int MM){
    int range = ((MM+1)-mm);
    int random = rand()%range + mm;
    return random;
} // a function used to generate random integers

vector<int> chain_spin(int d){
    mt19937_64 e2(15);
    uniform_int_distribution<int> uniform_dist(0,1);
    vector<int> mar(d);
    for (int i=0;i<d;i++){
        int t = uniform_dist(e2);
        if (t==0){mar[i]=-1;}
        if (t==1){mar[i]=1;}
    }
    return mar;
}
//generates a random chain of 1s and -1s to set them later on a lattice
