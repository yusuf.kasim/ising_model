#include <iostream>
#include <cstdlib>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <random>
#include "particule.h"

using namespace std;

particle::particle(int s)
: spin(s)
{
}
particle::~particle(){
}

particle::particle(const particle & a){
    spin = a.GetSp();
    posx = a.GetX();
    posy = a.GetY();
    inq = a.GetINQ();
    kx = a.GetKx();
    ky = a.GetKy();
}

particle::particle(){
}

void particle::SetSp(int news){
    spin =news;
}

void particle::Set_XY(int x,int y){
    posx = x;
    posy = y;
}

void particle::Set_K(double k1, double k2){
    kx=k1;
    ky=k2;
}

void particle::Set_INQ(int ni){
    inq = ni;
}


void particle::flip_s(){
    spin = -spin;
}

int particle::flip_v(){
    return -spin;
}

