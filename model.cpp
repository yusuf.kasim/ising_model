#include <iostream>
#include <cstdlib>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <random>
#include <vector>
#include <queue>
#include <deque>
#include <list>
#include <complex>
#include "particule.h"
#include "model.h"
#include "functions.h"


using namespace std;

model::model(int dd, vector<particle> lisst,int JJ,double B,int IT,double H)
: beta(B)
, d(dd)
, lattice(d, vector<particle>(d))
, BU(d, vector<particle>(d))
, cluster(d, vector<int>(d))
, itterations(IT)
, h(H)
, J(JJ)
, mt(itterations)
{


    for(int i=0;i<d;i++){
        for(int j=0;j<d;j++){
            double ii = i * 2*M_PI/double(d);
            double jj = j * 2*M_PI/double(d);
            lattice[j][i] = lisst[j+(d*i)];
            lattice[j][i].Set_XY(j,i);
            lattice[j][i].Set_K(jj,ii);
            lattice[j][i].Set_INQ(0);
            cluster[j][i] = 0;
        }
    }
    for (int i=0;i<itterations;i++){
        mt[i] = 0;
    }
    E=0;
    Chi=0;
    Cv=0;
    clusterprob = 0;
    BU = lattice;
    ediff = 0;
    Csize = 0;

}

model::~model(){
}

void model::Calcul_E_T(){
    E=0;
    for(int i=0;i<d;i++){
        for(int j=0;j<d;j++){
            E+= Calcul_E_S(lattice[i][j]);
            //cout << E << endl;
        }
    }
}

void model::Calcul_M_T(){
    mag = 0;
    for(int i=0;i<d;i++){
        for(int j=0;j<d;j++){
            mag+= lattice[i][j].GetSp();
        }
    }
}

double model::Calcul_E_S(particle e){
    double ee = 0;
    vector<int> pos(2);
    for (int k=0;k<4;k++){
        //cout <<"look " <<k << " " << e.GetX() << " " << e.GetY() << endl;
        pos = Nbr(e,k);
        ee += -J*e.GetSp()*lattice[pos[0]][pos[1]].GetSp()-h*e.GetSp();
        //cout << "n " <<pos[0]<< " " <<pos[1]<<endl;
    }
    return ee;
}


void model::print_lattice(){
    for (int i=0;i<d;i++){
        for(int j=0;j<d;j++){
            if(lattice[j][i].GetSp()<0){cout << "[" <<lattice[j][i].GetSp()<< "]";}
            if(lattice[j][i].GetSp()>0){cout << "[ " <<lattice[j][i].GetSp()<< "]";}
        }
        cout << endl;
    }
}

void model::printpng(){
    cout << "P3" << endl;
    cout << d << " " << d << endl;
    cout << "255" << endl;
    for (int i=0;i<d;i++){
        for(int j=0;j<d;j++){
            if(lattice[j][i].GetSp()<0){cout << "255 255 255 ";}
            if(lattice[j][i].GetSp()>0){cout << "0 0 0 ";}
        }
        cout << endl;
    }
}

vector<int> model::Nbr(particle a,int k){
    vector<int> result(2);
    if (k==0){
            result[0]=a.GetX()+1;
            result[1]=a.GetY();
    }
    if (k==1){
            result[0]=a.GetX();
            result[1]=a.GetY()+1;
    }
    if (k==2){
            result[0]=a.GetX()-1;
            result[1]=a.GetY();
    }
    if (k==3){
            result[0]=a.GetX();
            result[1]=a.GetY()-1;
    }
    if(result[0]<0){result[0]=(d-1);}
    if(result[1]<0){result[1]=(d-1);}
    if(result[0]>(d-1)){result[0]=0;}
    if(result[1]>(d-1)){result[1]=0;}
    return result;
}

double model::flipping_proba(particle t){
    double eei = 0;
    double eef = 0;
    eei = Calcul_E_S(t);
    t.flip_s();
    eef = Calcul_E_S(t);
    t.flip_s();
    ediff = eef- eei;
    if ((eef-eei)>0){return exp(-beta*(eef-eei));}
    else {return 1;}
}

double model::proba_heat_bath(particle t){
    double eei = 0;
    double eef = 0;
    eei = Calcul_E_S(t);
    t.flip_s();
    eef = Calcul_E_S(t);
    t.flip_s();
    ediff = eef- eei;
    return exp(-beta*(eef-eei))/double(exp(-beta*(eef-eei))+1.0);
}

double model::cal_chi(int si){
    double smt=0;
    double smt1 = 0;
    double smt2 = 0;
    for (int i =0;i<(itterations-si);i++){
        smt += mt[i]*mt[i+si];
        smt1 += mt[i];
        smt2 += mt[i+si];
    }
    double big= pow(1/double(itterations-si),2)*smt1*smt2;
    return (1/double(itterations-si)*smt)-big;
}

void model::Calculate_all(){
    ofstream out1("energies.txt");
    ofstream out2("mag.txt");
    ofstream out6("error.txt");
    //ofstream out3("autocor.txt");
    //freopen("status.txt","w",stdout);
    Calcul_M_T();
    Calcul_E_T();
    for(int i=0;i<itterations;i++){
        flipping();

        /*string FiNa = to_string(i)+".ppm";
        freopen(FiNa.c_str(),"w",stdout);
        printpng();*/

        mt[i] = fabs(mag/double(d*d));
        out1 << i << " " << E << endl;
        out2 << i << " " << fabs(mag) << endl;
        //cout << "itteration " << i << endl;
        //print_lattice();
    }
    em = em/double(itterations);
    em2 = em2/double(itterations);

    double err = sqrt(1/double(itterations))*sqrt(em2 - pow(em,2));
    out6 << err << endl;

    /*double s=0;
    double xi0 = cal_chi(0);
    for(int i=0;i<(itterations*0.8);i++){
        double xi = cal_chi(i);
        s+=xi/xi0;
        out3 << i << " " << xi/xi0 << endl;
    }
    s = 0.5+s;//double(itterations);
    freopen("CON","w",stdout);
    cout << s << endl;*/
}

void model::Cal_Heat_Bath(){
    ofstream out1("energies_HB.txt");
    ofstream out2("mag_HB.txt");
    ofstream out6("error_HB.txt");
    //ofstream out3("autocor_HB.txt");
    //freopen("status_HB.txt","w",stdout);
    Calcul_M_T();
    Calcul_E_T();
    em =0;
    em2=0;
    for(int i=0;i<itterations;i++){
        flipping_heat_bath();


        /*string FiNa = to_string(i)+".ppm";
        freopen(FiNa.c_str(),"w",stdout);
        printpng();*/

        mt[i] = fabs(mag/double(d*d));
        out1 << i << " " << E << endl;
        out2 << i << " " << fabs(mag) << endl;
        em += E;
        em2 += pow(E,2);
        //cout << "itteration " << i << endl;
        //print_lattice();
    }
    em = em/double(itterations);
    em2 = em2/double(itterations);

    double err = sqrt(1/double(itterations))*sqrt(em2 - pow(em,2));
    out6 << err << endl;
    /*freopen("CON","w",stdout);
    double s=0;
    double xi0 = cal_chi(0);
    for(int i=0;i<(itterations*0.8);i++){
        double xi = cal_chi(i);
        s+=xi/xi0;
        out3 << i << " " << xi/xi0 << endl;
    }
    s = 0.5+s;//double(itterations);
    cout << s << endl;*/
}

void model::Calculate_dif_B(){
    dt = 0.001;
    beta = 0;
    ofstream o1("e_b.txt");
    ofstream o2("m_b.txt");
    ofstream o3("chi_b.txt");
    ofstream o4("cv_b.txt");
    ofstream o5("em_b.txt");
    ofstream o6("mm_b.txt");
    do{
        em=0;
        em2=0;
        mm=0;
        mm2=0;/*
        if (beta > 0.01){dt = 0.0001;}
        if (beta > 0.1){dt = 0.001;}
        if (beta > 1){dt = 0.01;}
        if (beta > 10){dt = 0.1;}
        if (beta > 100){dt = 1;}*/
        lattice = BU;
        Calcul_M_T();
        Calcul_E_T();
        for(int i=0;i<itterations;i++){
            flipping();

            if(i>(itterations*0.5)){
                em +=E;
                em2+=pow(E,2);
                mm += fabs(mag/double(d*d));
                mm2 += pow(mag/double(d*d),2);
            }
        }
        em=em/double(itterations*0.5);
        em2=em2/double(itterations*0.5);
        mm=mm/double(itterations*0.5);
        mm2=mm2/double(itterations*0.5);

        /*Chi= beta*(d*d)*(mm2-pow(mm,2));
        Cv=pow(beta,2)/double(d*d)*(em2-pow(em,2));*/

        Chi  = beta *(mm2 - pow(mm,2));
        Cv = pow(beta,2)*(em2 - pow (em,2));

        o1 << beta << " " << E << endl;
        o2 << beta << " " << mag << endl;
        o3 << beta << " " << Chi << endl;
        o4 << beta << " " << Cv << endl;
        o5 << beta << " " << em << endl;
        o6 << beta << " " << mm << endl;

        /*int n = beta*1000;
        string FiNa = to_string(n)+".ppm";
        freopen(FiNa.c_str(),"w",stdout);
        printpng();*/

        beta+=dt;
    }while (beta<1);
}

void model::Calculate_dif_B_heat_bath(){
    double dt = 0.001;
    beta = 0;
    ofstream o1("e_b_HB.txt");
    ofstream o2("m_b_HB.txt");
    ofstream o3("chi_b_HB.txt");
    ofstream o4("cv_b_HB.txt");
    ofstream o5("em_b_HB.txt");
    ofstream o6("mm_b_HB.txt");
    do{
        double em=0;
        double em2=0;
        double mm=0;
        double mm2=0;/*
        if (beta > 0.01){dt = 0.0001;}
        if (beta > 0.1){dt = 0.001;}
        if (beta > 1){dt = 0.01;}
        if (beta > 10){dt = 0.1;}
        if (beta > 100){dt = 1;}*/
        lattice = BU;
        Calcul_M_T();
        Calcul_E_T();
        for(int i=0;i<itterations;i++){
            flipping_heat_bath();

            if(i>(itterations*0.5)){
                em +=E;
                em2+=pow(E,2);
                mm += fabs(mag/double(d*d));
                mm2 += pow(mag/double(d*d),2);
            }
        }
        em=em/double(itterations*0.5);
        em2=em2/double(itterations*0.5);
        mm=mm/double(itterations*0.5);
        mm2=mm2/double(itterations*0.5);

        /*Chi= beta*(d*d)*(mm2-pow(mm,2));
        Cv=pow(beta,2)/double(d*d)*(em2-pow(em,2));*/
        Chi  = beta *(mm2 - pow(mm,2));
        Cv = pow(beta,2)*(em2 - pow (em,2));

        o1 << beta << " " << E << endl;
        o2 << beta << " " << mag << endl;
        o3 << beta << " " << Chi << endl;
        o4 << beta << " " << Cv << endl;
        o5 << beta << " " << em << endl;
        o6 << beta << " " << mm << endl;

        /*int n = beta*1000;
        string FiNa = to_string(n)+".ppm";
        freopen(FiNa.c_str(),"w",stdout);
        printpng();*/

        beta+=dt;
    }while (beta<1);
}

void model::Calculate_dif_B_Cluster(){
    dt = 0.001;
    beta = 0;
    ofstream o1("e_b_c.txt");
    ofstream o2("m_b_c.txt");
    ofstream o3("chi_b_c.txt");
    ofstream o4("cv_b_c.txt");
    ofstream o5("em_b_c.txt");
    ofstream o6("mm_b_c.txt");
    ofstream o7("cl_size.txt");
    ofstream o8("mm2_b_c.txt");
    //ofstream o7("probs.txt");



    do{
        em=0;
        em2=0;
        mm=0;
        mm2=0;

        /*
        if (beta > 0.01){dt = 0.0001;}
        if (beta > 0.1){dt = 0.001;}
        if (beta > 1){dt = 0.01;}
        if (beta > 10){dt = 0.1;}
        if (beta > 100){dt = 1;}*/
        lattice = BU;
        Calcul_E_T();
        Calcul_M_T();
        double CS_A = 0;
        for(int i=0;i<itterations;i++){
            for(int i=0;i<d;i++){
                for (int j=0;j<d;j++){
                    lattice[i][j].Set_INQ(0);
                }
            }
            int pos1 = rand_int(0,d-1);
            int pos2 = rand_int(0,d-1);
            if(J>0){
            if (randd(0,1)>0.5){Csize = 0; clustering(lattice[pos1][pos2]);}
                while(! CL.empty()){
                    clustering(CL.front());
                    CL.pop();
                }
            }
            if(J<0){
                if (randd(0,1)>0.5){Csize = 0; clusteringNJ(lattice[pos1][pos2]);}
                while(! CL.empty()){
                    clusteringNJ(CL.front());
                    CL.pop();
                }
            }
            if (i> (itterations*0.5)){
                em +=E;
                em2+=pow(E,2);
                mm += fabs(mag/double(d*d));
                mm2 += pow(mag/double(d*d),2);
                CS_A += Csize;
            }
        }
        CS_A = CS_A / double(0.5*itterations*d*d);
        em=em/double(itterations*0.5);
        em2=em2/double(itterations*0.5);
        mm=mm/double(itterations*0.5);
        mm2=mm2/double(itterations*0.5);

        /*Chi= beta*(d*d)*(mm2-pow(mm,2));
        Cv=pow(beta,2)/double(d*d)*(em2-pow(em,2));
        */
        Chi  = beta * (mm2 - pow(mm,2));
        Cv = pow(beta,2)*(em2 - pow (em,2));

        o1 << beta << " " << E << endl;
        o2 << beta << " " << mag << endl;
        o3 << beta << " " << Chi << endl;
        o4 << beta << " " << Cv << endl;
        o5 << beta << " " << em << endl;
        o6 << beta << " " << mm << endl;
        o7 << beta << " "<< CS_A << endl;
        o8 << beta << " " << mm2 << endl;
        /*int n = beta*1000;
        string FiNa = to_string(n)+".ppm";
        freopen(FiNa.c_str(),"w",stdout);
        printpng();*/

        beta+=dt;
    }while (beta<1);
}

void model::Calculate_dif_T_Cluster(){ //AAAAAAAAAAAAAAAAAAAA here for different T and trying to rescale
    dt = 0.001;
    double T =0.5;

    ofstream o1("e_t_c.txt");
    ofstream o2("m_t_c.txt");
    ofstream o3("chi_t_c.txt");
    ofstream o4("cv_t_c.txt");
    ofstream o5("em_t_c.txt");
    ofstream o6("mm_t_c.txt");
    ofstream o7("cl_t_size.txt");
    ofstream o8("mm2_t_c.txt");
    //ofstream o7("probs.txt");



    do{
        em=0;
        em2=0;
        mm=0;
        mm2=0;
        beta = 1.0/T;

        lattice = BU;
        Calcul_E_T();
        Calcul_M_T();
        double CS_A = 0;
        for(int i=0;i<itterations;i++){
            for(int i=0;i<d;i++){
                for (int j=0;j<d;j++){
                    lattice[i][j].Set_INQ(0);
                }
            }
            int pos1 = rand_int(0,d-1);
            int pos2 = rand_int(0,d-1);
            if(J>0){
            if (randd(0,1)>0.5){Csize = 0; clustering(lattice[pos1][pos2]);}
                while(! CL.empty()){
                    clustering(CL.front());
                    CL.pop();
                }
            }
            if(J<0){
                if (randd(0,1)>0.5){Csize = 0; clusteringNJ(lattice[pos1][pos2]);}
                while(! CL.empty()){
                    clusteringNJ(CL.front());
                    CL.pop();
                }
            }
            if (i> (itterations*0.5)){
                em +=E;
                em2+=pow(E,2);
                mm += fabs(mag/double(d*d));
                mm2 += pow(mag/double(d*d),2);
                CS_A += Csize;
            }
        }
        CS_A = CS_A / double(0.5*itterations*d*d);
        em=em/double(itterations*0.5);
        em2=em2/double(itterations*0.5);
        mm=mm/double(itterations*0.5);
        mm2=mm2/double(itterations*0.5);

        Chi  = beta * (d*d) * (mm2 - pow(mm,2));
        Cv = pow(beta,2)/double(d*d)*(em2 - pow (em,2));

        o1 << T << " " << E << endl;
        o2 << T << " " << mag << endl;
        o3 << T << " " << Chi << endl;
        o4 << T << " " << Cv << endl;
        o5 << T << " " << em << endl;
        o6 << T << " " << mm << endl;
        o7 << T << " "<< CS_A << endl;
        o8 << T << " " << mm2 << endl;

        T+=dt;
    }while (T<4.5);
}



void model::Calculate_dif_H(){
    double dt = 0.001;
    h = 0;
    ofstream o1("e_h.txt");
    ofstream o2("m_h.txt");
    ofstream o3("chi_h.txt");
    ofstream o4("cv_h.txt");
    ofstream o5("em_h.txt");
    ofstream o6("mm_h.txt");

    do{
        double em=0;
        double em2=0;
        double mm=0;
        double mm2=0;
        Calcul_M_T();
        for(int i=0;i<itterations;i++){
            flipping();
            Calcul_E_T();
            em +=E;
            em2+=pow(E,2);
            mm += mag;
            mm2 += pow(mag,2);
        }
        em=em/double(itterations);
        em2=em2/double(itterations);
        mm=mm/double(itterations);
        mm2=mm2/double(itterations);

        Chi= beta*(d*d)*(mm2-pow(mm,2));
        Cv=pow(beta,2)/double(d*d)*(em2-pow(em,2));
        o1 << h << " " << E << endl;
        o2 << h << " " << mag << endl;
        o3 << h << " " << Chi << endl;
        o4 << h << " " << Cv << endl;
        o5 << h << " " << em << endl;
        o6 << h << " " << mm << endl;

        h+=dt;
    }while (h<0.5);
}

void model::Calculate_dif_H_heat_bath(){
    double dt = 0.01;
    h = -1;
    /*ostringstream test( " ");
    test << "mkdir results\\r_dif_h_HB_" << d << "_" << beta;
    string tt = test.str();
    system(tt.c_str() );
    ostringstream test2( " ");
    test2 << "D:\\Stage_ISIS_juin_2019\\Ising\\Ising_modele\\results\\r_dif_h_HB" << d << "_" << beta << "\\e_h.txt";
    string tt2 = test2.str();
*/
    ofstream o1("e_h_HB.txt");
    ofstream o2("m_h_HB.txt");
    ofstream o3("chi_h_HB.txt");
    ofstream o4("cv_h_HB.txt");
    ofstream o5("em_h_HB.txt");
    ofstream o6("mm_h_HB.txt");
    do{
        double em=0;
        double em2=0;
        double mm=0;
        double mm2=0;
        Calcul_E_T();
        Calcul_M_T();
        for(int i=0;i<itterations;i++){
            flipping_heat_bath();

            em +=E;
            em2+=pow(E,2);
            mm += fabs(mag/double(d*d));
            mm2 += pow(mag/double(d*d),2);
        }
        em=em/double(itterations);
        em2=em2/double(itterations);
        mm=mm/double(itterations);
        mm2=mm2/double(itterations);

        Chi= beta*(d*d)*(mm2-pow(mm,2));
        Cv=pow(beta,2)/double(d*d)*(em2-pow(em,2));
        o1 << h << " " << E << endl;
        o2 << h << " " << mag << endl;
        o3 << h << " " << Chi << endl;
        o4 << h << " " << Cv << endl;
        o5 << h << " " << em << endl;
        o6 << h << " " << mm << endl;

        h+=dt;
    }while (h<1);
}

void model::Calculate_dif_H_Cluster(){
    double dt = 0.001;
    h = 0;
    ofstream o1("e_h_c.txt");
    ofstream o2("m_h_c.txt");
    ofstream o3("chi_h_c.txt");
    ofstream o4("cv_h_c.txt");
    ofstream o5("em_h_c.txt");
    ofstream o6("mm_h_c.txt");

    do{
        double em=0;
        double em2=0;
        double mm=0;
        double mm2=0;
        for(int i=0;i<itterations;i++){
            int pos1 = rand_int(0,d-1);
            int pos2 = rand_int(0,d-1);
            //if (randd(0,1)>0.5){
            clustering(lattice[pos1][pos2]);
            Calcul_E_T();
            Calcul_M_T();
            em +=E;
            em2+=pow(E,2);
            mm += mag;
            mm2 += pow(mag,2);
        }
        em=em/double(itterations);
        em2=em2/double(itterations);
        mm=mm/double(itterations);
        mm2=mm2/double(itterations);

        /*Chi= beta*(d*d)*(mm2-pow(mm,2));
        Cv=pow(beta,2)/double(d*d)*(em2-pow(em,2));*/
        Chi  = beta *(mm2 - pow(mm,2));
        Cv = pow(beta,2)*(em2 - pow (em,2));

        o1 << h << " " << E << endl;
        o2 << h << " " << mag << endl;
        o3 << h << " " << Chi << endl;
        o4 << h << " " << Cv << endl;
        o5 << h << " " << em << endl;
        o6 << h << " " << mm << endl;


        h+=dt;
    }while (h<0.5);
}


void model::flipping(){
    int flipposx = rand_int(0,(d-1));
    int flipposy = rand_int(0,(d-1));
    double Upsilon = randd(0,1);
    double proba = flipping_proba(lattice[flipposx][flipposy]);
    if (proba>Upsilon){lattice[flipposx][flipposy].flip_s(); mag += 2*lattice[flipposx][flipposy].GetSp();E+=ediff;}
}

void model::flipping_heat_bath(){
    int flipposx = rand_int(0,(d-1));
    int flipposy = rand_int(0,(d-1));
    double Upsilon = randd(0,1);
    double proba = proba_heat_bath(lattice[flipposx][flipposy]);
    if (proba>Upsilon){lattice[flipposx][flipposy].flip_s(); mag += 2*lattice[flipposx][flipposy].GetSp();E+=ediff;}
}


void model::clustering(particle s){
    clusterprob = 1.0-(exp(-2*J*beta));
    double eei = 0;
    double eef = 0;
    eei = Calcul_E_S(s);
    s.flip_s();
    eef = Calcul_E_S(s);
    s.flip_s();
    ediff = eef- eei;

    vector<int> pos(2);
    for (int i=0;i<4;i++){
        pos = Nbr(s,i);
        double upsilon = randd(0,1);
        if(clusterprob>upsilon && s.GetSp() == lattice[pos[0]][pos[1]].GetSp() && lattice[pos[0]][pos[1]].GetINQ() ==0 ){
            CL.push(lattice[pos[0]][pos[1]]);
            lattice[pos[0]][pos[1]].Set_INQ(1);
        }
    }
    lattice[s.GetX()][s.GetY()].flip_s();
    mag += 2*lattice[s.GetX()][s.GetY()].GetSp();
    E += ediff;
    Csize += 1;
}


void model::clusteringNJ(particle s){
    clusterprob = 1-(exp(2*J*beta));
    double eei = 0;
    double eef = 0;
    eei = Calcul_E_S(s);
    s.flip_s();
    eef = Calcul_E_S(s);
    s.flip_s();
    ediff = eef- eei;

    vector<int> pos(2);
    for (int i=0;i<4;i++){
        pos = Nbr(s,i);
        double upsilon = randd(0,1);
        if(clusterprob>upsilon && (-s.GetSp()) == lattice[pos[0]][pos[1]].GetSp() && lattice[pos[0]][pos[1]].GetINQ() == 0 ){
            CL.push(lattice[pos[0]][pos[1]]);
            lattice[pos[0]][pos[1]].Set_INQ(1);
        }
    }
    lattice[s.GetX()][s.GetY()].flip_s();
    mag += 2*lattice[s.GetX()][s.GetY()].GetSp();
    E += ediff;
    Csize += 1;
}


void model::Calculate_cluster(){
    ofstream out1("energies_c.txt");
    ofstream out2("mag_c.txt");
    ofstream out3("autocor_c.txt");
    ofstream out4("cluseters.txt");
    ofstream out5("magpersping.txt");
    ofstream out6("error_c.txt");
    //freopen("status_c.txt","w",stdout);
    Calcul_E_T();
    Calcul_M_T();
    for(int i=0;i<itterations;i++){

        em = 0;
        em2 = 0;
        int pos1 = rand_int(0,d-1);
        int pos2 = rand_int(0,d-1);
        //cout << "ok";
        for (int i =0; i<d;i++){
            for(int j =0; j<d;j++){
                lattice[i][j].Set_INQ(0);
            }
        }

        if(J>0){
            if (randd(0,1)>0.5){Csize = 0; clustering(lattice[pos1][pos2]);}
            while(! CL.empty()){
                clustering(CL.front());
                CL.pop();
            }
        }
        if(J<0){
            if (randd(0,1)>0.5){Csize = 0; clusteringNJ(lattice[pos1][pos2]);}
            while(! CL.empty()){
                clusteringNJ(CL.front());
                CL.pop();
            }
        }


        /*string fn = to_string(i)+".ppm";  //this with printpng() allows you to create gifs
        freopen(fn.c_str(),"w",stdout);
        printpng();*/
        mt[i] = fabs(mag/double(d*d));
        em += E;
        em2 += pow(E,2);

        out1 << i << " " << E << endl;
        out2 << i << " " << fabs(mag) << endl;
        out4 << i << " " << Csize << endl;
        out5 << i << " " << mt[i] << endl;
        //cout << "itteration " << i << endl;
        //print_lattice();
    }
    em = em/double(itterations);
    em2 = em2/double(itterations);

    double err = sqrt(1/double(itterations))*sqrt(em2 - pow(em,2));
    out6 << err << endl;
    /*double s=0;
    double xi0 = cal_chi(0);
    for(int i=0;i<(itterations*0.8);i++){
        double xi = cal_chi(i);
        s+=xi/xi0;
        out3 << i << " " << xi/xi0 << endl;
    }
    s = 0.5+s;//double(itterations);
    freopen("CON","w",stdout);
    cout << s << endl;*/
}

void model::calc_fors(){
    ofstream oo ("forms.txt");
    for (int i=0;i<d;i++){
        for (int j=0;j<d;j++){
            double St = forms_S(lattice[i][j]);
            oo <<  lattice[i][j].GetKx() << " " << lattice[i][j].GetKy() << " " << St << endl;
        }
    }


}

double model::forms_S(particle aa){
    double re = 0;
    double im = 0;
    for(int i=0;i<d;i++){
        for (int j=0;j<d;j++){
            double p = (aa.GetKx()*lattice[i][j].GetX())+(aa.GetKy()*lattice[i][j].GetY());
            re += cos(p)*lattice[i][j].GetSp();
            im += sin(p)*lattice[i][j].GetSp();
            //cout << p << " " <<re << " " << im << endl;
        }
    }
    double S = (re*re+im*im)/pow(double(d*d),2);
    //cout << S << endl;
    return S;

}

void model::show_cluster(){
    for (int i =0; i<d;i++){
        for(int j =0; j<d;j++){
            cout << cluster[j][i];
        }cout << endl;
    }
}
/*for(int k =0; k<d;k++){
                    for (int l =0; l<d; l++){
            */
